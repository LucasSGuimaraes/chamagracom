object DM: TDM
  OldCreateOrder = False
  Height = 223
  Width = 331
  object qryAuxiliar1: TFDQuery
    Connection = con
    Left = 144
    Top = 80
  end
  object con: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\Lucas\Projetos\Delphi\ChamaGarcom\db\GESTOR.SD' +
        'B'
      'DriverID=SQLite')
    LoginPrompt = False
    Transaction = trs
    BeforeConnect = conBeforeConnect
    Left = 144
    Top = 16
  end
  object link: TFDPhysSQLiteDriverLink
    Left = 256
    Top = 16
  end
  object cursor: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 256
    Top = 80
  end
  object trs: TFDTransaction
    Connection = con
    Left = 200
    Top = 16
  end
  object qryConfig: TFDQuery
    Connection = con
    Transaction = trs
    SQL.Strings = (
      'SELECT * FROM CONFIG')
    Left = 200
    Top = 80
    object qryConfigHOST: TWideMemoField
      FieldName = 'HOST'
      Origin = 'HOST'
      BlobType = ftWideMemo
    end
    object qryConfigTIPO_CONTA: TStringField
      FieldName = 'TIPO_CONTA'
      Origin = 'TIPO_CONTA'
      Size = 10
    end
  end
  object DSRestConnection: TDSRestConnection
    Host = '192.168.100.109'
    Port = 8080
    LoginPrompt = False
    Left = 56
    Top = 16
    UniqueId = '{2C8A74D2-E41A-4CCC-9DDF-7D2BFE954AF3}'
  end
  object FDStanStorageBinLink: TFDStanStorageBinLink
    Left = 56
    Top = 80
  end
  object FDStanStorageJSONLink: TFDStanStorageJSONLink
    Left = 56
    Top = 144
  end
end
