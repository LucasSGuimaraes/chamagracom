unit uDM;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, FireDAC.FMXUI.Wait, Data.DB,
  FireDAC.Comp.Client, FireDAC.Comp.UI, FireDAC.Comp.DataSet,
  FireDAC.Stan.StorageJSON, FireDAC.Stan.StorageBin, Datasnap.DSClientRest,
  uProxy;

type
  TDM = class(TDataModule)
    qryAuxiliar1: TFDQuery;
    con: TFDConnection;
    link: TFDPhysSQLiteDriverLink;
    cursor: TFDGUIxWaitCursor;
    trs: TFDTransaction;
    qryConfig: TFDQuery;
    qryConfigHOST: TWideMemoField;
    qryConfigTIPO_CONTA: TStringField;
    DSRestConnection: TDSRestConnection;
    FDStanStorageBinLink: TFDStanStorageBinLink;
    FDStanStorageJSONLink: TFDStanStorageJSONLink;
    procedure conBeforeConnect(Sender: TObject);
  private
    FSrvServerMetodosClient: TSrvServerMetodosClient;
    FInstanceOwner: Boolean;
    function GetSrvServerMetodosClient: TSrvServerMetodosClient;
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property InstanceOwner: Boolean read FInstanceOwner write FInstanceOwner;
    property SrvServerMetodosClient: TSrvServerMetodosClient read GetSrvServerMetodosClient write FSrvServerMetodosClient;
  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

uses
  System.IOUtils;

procedure TDM.conBeforeConnect(Sender: TObject);
begin
{$IFDEF ANDROID}
  con.Params.Values['Database'] := TPath.GetDocumentsPath + PathDelim +
    'GESTOR.SDB';
{$ENDIF}
end;

constructor TDM.Create(AOwner: TComponent);
begin
  inherited;
  FInstanceOwner := True;
end;

destructor TDM.Destroy;
begin
  FSrvServerMetodosClient.Free;
  inherited;
end;

function TDM.GetSrvServerMetodosClient: TSrvServerMetodosClient;
begin
  if FSrvServerMetodosClient = nil then
    FSrvServerMetodosClient:= TSrvServerMetodosClient.Create(DSRestConnection, FInstanceOwner);
  Result := FSrvServerMetodosClient;
end;

end.
