//
// Created by the DataSnap proxy generator.
// 12/05/2017 14:29:15
//

unit uProxy;

interface

uses System.JSON, Datasnap.DSProxyRest, Datasnap.DSClientRest, Data.DBXCommon,
  Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON, Datasnap.DSProxy,
  System.Classes, System.SysUtils, Data.DB, Data.SqlExpr, Data.DBXDBReaders,
  Data.DBXCDSReaders, System.Win.ScktComp, Data.FireDACJSONReflect,
  Data.DBXJSONReflect;

type

  IDSRestCachedTFDJSONDataSets = interface;

  TSrvServerMetodosClient = class(TDSAdminRestClient)
  private
    FClientSocket1ReadCommand: TDSRestCommand;
    FEchoStringCommand: TDSRestCommand;
    FReverseStringCommand: TDSRestCommand;
    FGetGruposCommand: TDSRestCommand;
    FGetGruposCommand_Cache: TDSRestCommand;
    FGetUsuariosCommand: TDSRestCommand;
    FGetUsuariosCommand_Cache: TDSRestCommand;
    FGetFavoritosCommand: TDSRestCommand;
    FGetFavoritosCommand_Cache: TDSRestCommand;
    FGetProdutosCommand: TDSRestCommand;
    FGetProdutosCommand_Cache: TDSRestCommand;
    FEnviarBDCommand: TDSRestCommand;
    FEnviarBDCommand_Cache: TDSRestCommand;
    FPutProdutosCommand: TDSRestCommand;
    FGetContaCommand: TDSRestCommand;
    FGetContaCommand_Cache: TDSRestCommand;
    FValidaLoginCommand: TDSRestCommand;
    FValidaMesaCommand: TDSRestCommand;
    FTrocaMesaCommand: TDSRestCommand;
    FTransferirItemMesaCommand: TDSRestCommand;
    FImprimirItemProducaoCommand: TDSRestCommand;
    FImprimirTodosItensProducaoCommand: TDSRestCommand;
    FImprimirContaCommand: TDSRestCommand;
  public
    constructor Create(ARestConnection: TDSRestConnection); overload;
    constructor Create(ARestConnection: TDSRestConnection;
      AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    procedure ClientSocket1Read(Sender: TObject; Socket: TCustomWinSocket);
    function EchoString(Value: string;
      const ARequestFilter: string = ''): string;
    function ReverseString(Value: string;
      const ARequestFilter: string = ''): string;
    function GetGrupos(const ARequestFilter: string = ''): TFDJSONDataSets;
    function GetGrupos_Cache(const ARequestFilter: string = '')
      : IDSRestCachedTFDJSONDataSets;
    function GetUsuarios(const ARequestFilter: string = ''): TFDJSONDataSets;
    function GetUsuarios_Cache(const ARequestFilter: string = '')
      : IDSRestCachedTFDJSONDataSets;
    function GetFavoritos(const ARequestFilter: string = ''): TFDJSONDataSets;
    function GetFavoritos_Cache(const ARequestFilter: string = '')
      : IDSRestCachedTFDJSONDataSets;
    function GetProdutos(Grupo: Integer; const ARequestFilter: string = '')
      : TFDJSONDataSets;
    function GetProdutos_Cache(Grupo: Integer;
      const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function EnviarBD(const ARequestFilter: string = ''): TFDJSONDataSets;
    function EnviarBD_Cache(const ARequestFilter: string = '')
      : IDSRestCachedTFDJSONDataSets;
    function PutProdutos(ListaProdutos: TFDJSONDataSets;
      const ARequestFilter: string = ''): Boolean;
    function GetConta(Comanda: Integer; const ARequestFilter: string = '')
      : TFDJSONDataSets;
    function GetConta_Cache(Comanda: Integer; const ARequestFilter: string = '')
      : IDSRestCachedTFDJSONDataSets;
    function ValidaLogin(Login: string; Senha: string;
      const ARequestFilter: string = ''): Boolean;
    function ValidaMesa(Mesa: Integer;
      const ARequestFilter: string = ''): Boolean;
    function TrocaMesa(MesaAtual: Integer; MesaNova: Integer;
      const ARequestFilter: string = ''): Boolean;
    function TransferirItemMesa(Item: Integer; MesaAtual: Integer;
      MesaNova: Integer; const ARequestFilter: string = ''): Boolean;
    function ImprimirItemProducao(Comanda: string; Item: string;
      const ARequestFilter: string = ''): Boolean;
    function ImprimirTodosItensProducao(Comanda: string;
      const ARequestFilter: string = ''): Boolean;
    function ImprimirConta(Comanda: string;
      const ARequestFilter: string = ''): Boolean;
  end;

  IDSRestCachedTFDJSONDataSets = interface(IDSRestCachedObject<TFDJSONDataSets>)
  end;

  TDSRestCachedTFDJSONDataSets = class(TDSRestCachedObject<TFDJSONDataSets>,
    IDSRestCachedTFDJSONDataSets, IDSRestCachedCommand)
  end;

const
  TSrvServerMetodos_ClientSocket1Read: array [0 .. 1]
    of TDSRestParameterMetaData = ((Name: 'Sender'; Direction: 1; DBXType: 37;
    TypeName: 'TObject'), (Name: 'Socket'; Direction: 1; DBXType: 37;
    TypeName: 'TCustomWinSocket'));

  TSrvServerMetodos_EchoString: array [0 .. 1] of TDSRestParameterMetaData =
    ((Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'), (Name: '';
    Direction: 4; DBXType: 26; TypeName: 'string'));

  TSrvServerMetodos_ReverseString: array [0 .. 1] of TDSRestParameterMetaData =
    ((Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'), (Name: '';
    Direction: 4; DBXType: 26; TypeName: 'string'));

  TSrvServerMetodos_GetGrupos: array [0 .. 0] of TDSRestParameterMetaData =
    ((Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets'));

  TSrvServerMetodos_GetGrupos_Cache: array [0 .. 0]
    of TDSRestParameterMetaData = ((Name: ''; Direction: 4; DBXType: 26;
    TypeName: 'String'));

  TSrvServerMetodos_GetUsuarios: array [0 .. 0] of TDSRestParameterMetaData =
    ((Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets'));

  TSrvServerMetodos_GetUsuarios_Cache: array [0 .. 0]
    of TDSRestParameterMetaData = ((Name: ''; Direction: 4; DBXType: 26;
    TypeName: 'String'));

  TSrvServerMetodos_GetFavoritos: array [0 .. 0] of TDSRestParameterMetaData =
    ((Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets'));

  TSrvServerMetodos_GetFavoritos_Cache: array [0 .. 0]
    of TDSRestParameterMetaData = ((Name: ''; Direction: 4; DBXType: 26;
    TypeName: 'String'));

  TSrvServerMetodos_GetProdutos: array [0 .. 1] of TDSRestParameterMetaData =
    ((Name: 'Grupo'; Direction: 1; DBXType: 6; TypeName: 'Integer'), (Name: '';
    Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets'));

  TSrvServerMetodos_GetProdutos_Cache: array [0 .. 1]
    of TDSRestParameterMetaData = ((Name: 'Grupo'; Direction: 1; DBXType: 6;
    TypeName: 'Integer'), (Name: ''; Direction: 4; DBXType: 26;
    TypeName: 'String'));

  TSrvServerMetodos_EnviarBD: array [0 .. 0] of TDSRestParameterMetaData =
    ((Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets'));

  TSrvServerMetodos_EnviarBD_Cache: array [0 .. 0] of TDSRestParameterMetaData =
    ((Name: ''; Direction: 4; DBXType: 26; TypeName: 'String'));

  TSrvServerMetodos_PutProdutos: array [0 .. 1] of TDSRestParameterMetaData =
    ((Name: 'ListaProdutos'; Direction: 1; DBXType: 37;
    TypeName: 'TFDJSONDataSets'), (Name: ''; Direction: 4; DBXType: 4;
    TypeName: 'Boolean'));

  TSrvServerMetodos_GetConta: array [0 .. 1] of TDSRestParameterMetaData =
    ((Name: 'Comanda'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets'));

  TSrvServerMetodos_GetConta_Cache: array [0 .. 1] of TDSRestParameterMetaData =
    ((Name: 'Comanda'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String'));

  TSrvServerMetodos_ValidaLogin: array [0 .. 2] of TDSRestParameterMetaData =
    ((Name: 'Login'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'Senha'; Direction: 1; DBXType: 26; TypeName: 'string'), (Name: '';
    Direction: 4; DBXType: 4; TypeName: 'Boolean'));

  TSrvServerMetodos_ValidaMesa: array [0 .. 1] of TDSRestParameterMetaData =
    ((Name: 'Mesa'; Direction: 1; DBXType: 6; TypeName: 'Integer'), (Name: '';
    Direction: 4; DBXType: 4; TypeName: 'Boolean'));

  TSrvServerMetodos_TrocaMesa: array [0 .. 2] of TDSRestParameterMetaData =
    ((Name: 'MesaAtual'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'MesaNova'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 4; TypeName: 'Boolean'));

  TSrvServerMetodos_TransferirItemMesa: array [0 .. 3]
    of TDSRestParameterMetaData = ((Name: 'Item'; Direction: 1; DBXType: 6;
    TypeName: 'Integer'), (Name: 'MesaAtual'; Direction: 1; DBXType: 6;
    TypeName: 'Integer'), (Name: 'MesaNova'; Direction: 1; DBXType: 6;
    TypeName: 'Integer'), (Name: ''; Direction: 4; DBXType: 4;
    TypeName: 'Boolean'));

  TSrvServerMetodos_ImprimirItemProducao: array [0 .. 2]
    of TDSRestParameterMetaData = ((Name: 'Comanda'; Direction: 1; DBXType: 26;
    TypeName: 'string'), (Name: 'Item'; Direction: 1; DBXType: 26;
    TypeName: 'string'), (Name: ''; Direction: 4; DBXType: 4;
    TypeName: 'Boolean'));

  TSrvServerMetodos_ImprimirTodosItensProducao: array [0 .. 1]
    of TDSRestParameterMetaData = ((Name: 'Comanda'; Direction: 1; DBXType: 26;
    TypeName: 'string'), (Name: ''; Direction: 4; DBXType: 4;
    TypeName: 'Boolean'));

  TSrvServerMetodos_ImprimirConta: array [0 .. 1] of TDSRestParameterMetaData =
    ((Name: 'Comanda'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 4; TypeName: 'Boolean'));

implementation

procedure TSrvServerMetodosClient.ClientSocket1Read(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  if FClientSocket1ReadCommand = nil then
  begin
    FClientSocket1ReadCommand := FConnection.CreateCommand;
    FClientSocket1ReadCommand.RequestType := 'POST';
    FClientSocket1ReadCommand.Text := 'TSrvServerMetodos."ClientSocket1Read"';
    FClientSocket1ReadCommand.Prepare(TSrvServerMetodos_ClientSocket1Read);
  end;
  if not Assigned(Sender) then
    FClientSocket1ReadCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDSRestCommand(FClientSocket1ReadCommand.Parameters[0]
      .ConnectionHandler).GetJSONMarshaler;
    try
      FClientSocket1ReadCommand.Parameters[0].Value.SetJSONValue
        (FMarshal.Marshal(Sender), True);
      if FInstanceOwner then
        Sender.Free
    finally
      FreeAndNil(FMarshal)
    end
  end;
  if not Assigned(Socket) then
    FClientSocket1ReadCommand.Parameters[1].Value.SetNull
  else
  begin
    FMarshal := TDSRestCommand(FClientSocket1ReadCommand.Parameters[1]
      .ConnectionHandler).GetJSONMarshaler;
    try
      FClientSocket1ReadCommand.Parameters[1].Value.SetJSONValue
        (FMarshal.Marshal(Socket), True);
      if FInstanceOwner then
        Socket.Free
    finally
      FreeAndNil(FMarshal)
    end
  end;
  FClientSocket1ReadCommand.Execute;
end;

function TSrvServerMetodosClient.EchoString(Value: string;
  const ARequestFilter: string): string;
begin
  if FEchoStringCommand = nil then
  begin
    FEchoStringCommand := FConnection.CreateCommand;
    FEchoStringCommand.RequestType := 'GET';
    FEchoStringCommand.Text := 'TSrvServerMetodos.EchoString';
    FEchoStringCommand.Prepare(TSrvServerMetodos_EchoString);
  end;
  FEchoStringCommand.Parameters[0].Value.SetWideString(Value);
  FEchoStringCommand.Execute(ARequestFilter);
  Result := FEchoStringCommand.Parameters[1].Value.GetWideString;
end;

function TSrvServerMetodosClient.ReverseString(Value: string;
  const ARequestFilter: string): string;
begin
  if FReverseStringCommand = nil then
  begin
    FReverseStringCommand := FConnection.CreateCommand;
    FReverseStringCommand.RequestType := 'GET';
    FReverseStringCommand.Text := 'TSrvServerMetodos.ReverseString';
    FReverseStringCommand.Prepare(TSrvServerMetodos_ReverseString);
  end;
  FReverseStringCommand.Parameters[0].Value.SetWideString(Value);
  FReverseStringCommand.Execute(ARequestFilter);
  Result := FReverseStringCommand.Parameters[1].Value.GetWideString;
end;

function TSrvServerMetodosClient.GetGrupos(const ARequestFilter: string)
  : TFDJSONDataSets;
begin
  if FGetGruposCommand = nil then
  begin
    FGetGruposCommand := FConnection.CreateCommand;
    FGetGruposCommand.RequestType := 'GET';
    FGetGruposCommand.Text := 'TSrvServerMetodos.GetGrupos';
    FGetGruposCommand.Prepare(TSrvServerMetodos_GetGrupos);
  end;
  FGetGruposCommand.Execute(ARequestFilter);
  if not FGetGruposCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetGruposCommand.Parameters[0]
      .ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets
        (FUnMarshal.UnMarshal(FGetGruposCommand.Parameters[0]
        .Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetGruposCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSrvServerMetodosClient.GetGrupos_Cache(const ARequestFilter: string)
  : IDSRestCachedTFDJSONDataSets;
begin
  if FGetGruposCommand_Cache = nil then
  begin
    FGetGruposCommand_Cache := FConnection.CreateCommand;
    FGetGruposCommand_Cache.RequestType := 'GET';
    FGetGruposCommand_Cache.Text := 'TSrvServerMetodos.GetGrupos';
    FGetGruposCommand_Cache.Prepare(TSrvServerMetodos_GetGrupos_Cache);
  end;
  FGetGruposCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create
    (FGetGruposCommand_Cache.Parameters[0].Value.GetString);
end;

function TSrvServerMetodosClient.GetUsuarios(const ARequestFilter: string)
  : TFDJSONDataSets;
begin
  if FGetUsuariosCommand = nil then
  begin
    FGetUsuariosCommand := FConnection.CreateCommand;
    FGetUsuariosCommand.RequestType := 'GET';
    FGetUsuariosCommand.Text := 'TSrvServerMetodos.GetUsuarios';
    FGetUsuariosCommand.Prepare(TSrvServerMetodos_GetUsuarios);
  end;
  FGetUsuariosCommand.Execute(ARequestFilter);
  if not FGetUsuariosCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetUsuariosCommand.Parameters[0]
      .ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets
        (FUnMarshal.UnMarshal(FGetUsuariosCommand.Parameters[0]
        .Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetUsuariosCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSrvServerMetodosClient.GetUsuarios_Cache(const ARequestFilter: string)
  : IDSRestCachedTFDJSONDataSets;
begin
  if FGetUsuariosCommand_Cache = nil then
  begin
    FGetUsuariosCommand_Cache := FConnection.CreateCommand;
    FGetUsuariosCommand_Cache.RequestType := 'GET';
    FGetUsuariosCommand_Cache.Text := 'TSrvServerMetodos.GetUsuarios';
    FGetUsuariosCommand_Cache.Prepare(TSrvServerMetodos_GetUsuarios_Cache);
  end;
  FGetUsuariosCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create
    (FGetUsuariosCommand_Cache.Parameters[0].Value.GetString);
end;

function TSrvServerMetodosClient.GetFavoritos(const ARequestFilter: string)
  : TFDJSONDataSets;
begin
  if FGetFavoritosCommand = nil then
  begin
    FGetFavoritosCommand := FConnection.CreateCommand;
    FGetFavoritosCommand.RequestType := 'GET';
    FGetFavoritosCommand.Text := 'TSrvServerMetodos.GetFavoritos';
    FGetFavoritosCommand.Prepare(TSrvServerMetodos_GetFavoritos);
  end;
  FGetFavoritosCommand.Execute(ARequestFilter);
  if not FGetFavoritosCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetFavoritosCommand.Parameters[0]
      .ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets
        (FUnMarshal.UnMarshal(FGetFavoritosCommand.Parameters[0]
        .Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetFavoritosCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSrvServerMetodosClient.GetFavoritos_Cache(const ARequestFilter
  : string): IDSRestCachedTFDJSONDataSets;
begin
  if FGetFavoritosCommand_Cache = nil then
  begin
    FGetFavoritosCommand_Cache := FConnection.CreateCommand;
    FGetFavoritosCommand_Cache.RequestType := 'GET';
    FGetFavoritosCommand_Cache.Text := 'TSrvServerMetodos.GetFavoritos';
    FGetFavoritosCommand_Cache.Prepare(TSrvServerMetodos_GetFavoritos_Cache);
  end;
  FGetFavoritosCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create
    (FGetFavoritosCommand_Cache.Parameters[0].Value.GetString);
end;

function TSrvServerMetodosClient.GetProdutos(Grupo: Integer;
  const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGetProdutosCommand = nil then
  begin
    FGetProdutosCommand := FConnection.CreateCommand;
    FGetProdutosCommand.RequestType := 'GET';
    FGetProdutosCommand.Text := 'TSrvServerMetodos.GetProdutos';
    FGetProdutosCommand.Prepare(TSrvServerMetodos_GetProdutos);
  end;
  FGetProdutosCommand.Parameters[0].Value.SetInt32(Grupo);
  FGetProdutosCommand.Execute(ARequestFilter);
  if not FGetProdutosCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetProdutosCommand.Parameters[1]
      .ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets
        (FUnMarshal.UnMarshal(FGetProdutosCommand.Parameters[1]
        .Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetProdutosCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSrvServerMetodosClient.GetProdutos_Cache(Grupo: Integer;
  const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGetProdutosCommand_Cache = nil then
  begin
    FGetProdutosCommand_Cache := FConnection.CreateCommand;
    FGetProdutosCommand_Cache.RequestType := 'GET';
    FGetProdutosCommand_Cache.Text := 'TSrvServerMetodos.GetProdutos';
    FGetProdutosCommand_Cache.Prepare(TSrvServerMetodos_GetProdutos_Cache);
  end;
  FGetProdutosCommand_Cache.Parameters[0].Value.SetInt32(Grupo);
  FGetProdutosCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create
    (FGetProdutosCommand_Cache.Parameters[1].Value.GetString);
end;

function TSrvServerMetodosClient.EnviarBD(const ARequestFilter: string)
  : TFDJSONDataSets;
begin
  if FEnviarBDCommand = nil then
  begin
    FEnviarBDCommand := FConnection.CreateCommand;
    FEnviarBDCommand.RequestType := 'GET';
    FEnviarBDCommand.Text := 'TSrvServerMetodos.EnviarBD';
    FEnviarBDCommand.Prepare(TSrvServerMetodos_EnviarBD);
  end;
  FEnviarBDCommand.Execute(ARequestFilter);
  if not FEnviarBDCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FEnviarBDCommand.Parameters[0]
      .ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FEnviarBDCommand.Parameters
        [0].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FEnviarBDCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSrvServerMetodosClient.EnviarBD_Cache(const ARequestFilter: string)
  : IDSRestCachedTFDJSONDataSets;
begin
  if FEnviarBDCommand_Cache = nil then
  begin
    FEnviarBDCommand_Cache := FConnection.CreateCommand;
    FEnviarBDCommand_Cache.RequestType := 'GET';
    FEnviarBDCommand_Cache.Text := 'TSrvServerMetodos.EnviarBD';
    FEnviarBDCommand_Cache.Prepare(TSrvServerMetodos_EnviarBD_Cache);
  end;
  FEnviarBDCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create
    (FEnviarBDCommand_Cache.Parameters[0].Value.GetString);
end;

function TSrvServerMetodosClient.PutProdutos(ListaProdutos: TFDJSONDataSets;
  const ARequestFilter: string): Boolean;
begin
  if FPutProdutosCommand = nil then
  begin
    FPutProdutosCommand := FConnection.CreateCommand;
    FPutProdutosCommand.RequestType := 'POST';
    FPutProdutosCommand.Text := 'TSrvServerMetodos."PutProdutos"';
    FPutProdutosCommand.Prepare(TSrvServerMetodos_PutProdutos);
  end;
  if not Assigned(ListaProdutos) then
    FPutProdutosCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDSRestCommand(FPutProdutosCommand.Parameters[0]
      .ConnectionHandler).GetJSONMarshaler;
    try
      FPutProdutosCommand.Parameters[0].Value.SetJSONValue
        (FMarshal.Marshal(ListaProdutos), True);
      if FInstanceOwner then
        ListaProdutos.Free
    finally
      FreeAndNil(FMarshal)
    end
  end;
  FPutProdutosCommand.Execute(ARequestFilter);
  Result := FPutProdutosCommand.Parameters[1].Value.GetBoolean;
end;

function TSrvServerMetodosClient.GetConta(Comanda: Integer;
  const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGetContaCommand = nil then
  begin
    FGetContaCommand := FConnection.CreateCommand;
    FGetContaCommand.RequestType := 'GET';
    FGetContaCommand.Text := 'TSrvServerMetodos.GetConta';
    FGetContaCommand.Prepare(TSrvServerMetodos_GetConta);
  end;
  FGetContaCommand.Parameters[0].Value.SetInt32(Comanda);
  FGetContaCommand.Execute(ARequestFilter);
  if not FGetContaCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetContaCommand.Parameters[1]
      .ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetContaCommand.Parameters
        [1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetContaCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TSrvServerMetodosClient.GetConta_Cache(Comanda: Integer;
  const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGetContaCommand_Cache = nil then
  begin
    FGetContaCommand_Cache := FConnection.CreateCommand;
    FGetContaCommand_Cache.RequestType := 'GET';
    FGetContaCommand_Cache.Text := 'TSrvServerMetodos.GetConta';
    FGetContaCommand_Cache.Prepare(TSrvServerMetodos_GetConta_Cache);
  end;
  FGetContaCommand_Cache.Parameters[0].Value.SetInt32(Comanda);
  FGetContaCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create
    (FGetContaCommand_Cache.Parameters[1].Value.GetString);
end;

function TSrvServerMetodosClient.ValidaLogin(Login: string; Senha: string;
  const ARequestFilter: string): Boolean;
begin
  if FValidaLoginCommand = nil then
  begin
    FValidaLoginCommand := FConnection.CreateCommand;
    FValidaLoginCommand.RequestType := 'GET';
    FValidaLoginCommand.Text := 'TSrvServerMetodos.ValidaLogin';
    FValidaLoginCommand.Prepare(TSrvServerMetodos_ValidaLogin);
  end;
  FValidaLoginCommand.Parameters[0].Value.SetWideString(Login);
  FValidaLoginCommand.Parameters[1].Value.SetWideString(Senha);
  FValidaLoginCommand.Execute(ARequestFilter);
  Result := FValidaLoginCommand.Parameters[2].Value.GetBoolean;
end;

function TSrvServerMetodosClient.ValidaMesa(Mesa: Integer;
  const ARequestFilter: string): Boolean;
begin
  if FValidaMesaCommand = nil then
  begin
    FValidaMesaCommand := FConnection.CreateCommand;
    FValidaMesaCommand.RequestType := 'GET';
    FValidaMesaCommand.Text := 'TSrvServerMetodos.ValidaMesa';
    FValidaMesaCommand.Prepare(TSrvServerMetodos_ValidaMesa);
  end;
  FValidaMesaCommand.Parameters[0].Value.SetInt32(Mesa);
  FValidaMesaCommand.Execute(ARequestFilter);
  Result := FValidaMesaCommand.Parameters[1].Value.GetBoolean;
end;

function TSrvServerMetodosClient.TrocaMesa(MesaAtual: Integer;
  MesaNova: Integer; const ARequestFilter: string): Boolean;
begin
  if FTrocaMesaCommand = nil then
  begin
    FTrocaMesaCommand := FConnection.CreateCommand;
    FTrocaMesaCommand.RequestType := 'GET';
    FTrocaMesaCommand.Text := 'TSrvServerMetodos.TrocaMesa';
    FTrocaMesaCommand.Prepare(TSrvServerMetodos_TrocaMesa);
  end;
  FTrocaMesaCommand.Parameters[0].Value.SetInt32(MesaAtual);
  FTrocaMesaCommand.Parameters[1].Value.SetInt32(MesaNova);
  FTrocaMesaCommand.Execute(ARequestFilter);
  Result := FTrocaMesaCommand.Parameters[2].Value.GetBoolean;
end;

function TSrvServerMetodosClient.TransferirItemMesa(Item: Integer;
  MesaAtual: Integer; MesaNova: Integer; const ARequestFilter: string): Boolean;
begin
  if FTransferirItemMesaCommand = nil then
  begin
    FTransferirItemMesaCommand := FConnection.CreateCommand;
    FTransferirItemMesaCommand.RequestType := 'GET';
    FTransferirItemMesaCommand.Text := 'TSrvServerMetodos.TransferirItemMesa';
    FTransferirItemMesaCommand.Prepare(TSrvServerMetodos_TransferirItemMesa);
  end;
  FTransferirItemMesaCommand.Parameters[0].Value.SetInt32(Item);
  FTransferirItemMesaCommand.Parameters[1].Value.SetInt32(MesaAtual);
  FTransferirItemMesaCommand.Parameters[2].Value.SetInt32(MesaNova);
  FTransferirItemMesaCommand.Execute(ARequestFilter);
  Result := FTransferirItemMesaCommand.Parameters[3].Value.GetBoolean;
end;

function TSrvServerMetodosClient.ImprimirItemProducao(Comanda: string;
  Item: string; const ARequestFilter: string): Boolean;
begin
  if FImprimirItemProducaoCommand = nil then
  begin
    FImprimirItemProducaoCommand := FConnection.CreateCommand;
    FImprimirItemProducaoCommand.RequestType := 'GET';
    FImprimirItemProducaoCommand.Text :=
      'TSrvServerMetodos.ImprimirItemProducao';
    FImprimirItemProducaoCommand.Prepare
      (TSrvServerMetodos_ImprimirItemProducao);
  end;
  FImprimirItemProducaoCommand.Parameters[0].Value.SetWideString(Comanda);
  FImprimirItemProducaoCommand.Parameters[1].Value.SetWideString(Item);
  FImprimirItemProducaoCommand.Execute(ARequestFilter);
  Result := FImprimirItemProducaoCommand.Parameters[2].Value.GetBoolean;
end;

function TSrvServerMetodosClient.ImprimirTodosItensProducao(Comanda: string;
  const ARequestFilter: string): Boolean;
begin
  if FImprimirTodosItensProducaoCommand = nil then
  begin
    FImprimirTodosItensProducaoCommand := FConnection.CreateCommand;
    FImprimirTodosItensProducaoCommand.RequestType := 'GET';
    FImprimirTodosItensProducaoCommand.Text :=
      'TSrvServerMetodos.ImprimirTodosItensProducao';
    FImprimirTodosItensProducaoCommand.Prepare
      (TSrvServerMetodos_ImprimirTodosItensProducao);
  end;
  FImprimirTodosItensProducaoCommand.Parameters[0].Value.SetWideString(Comanda);
  FImprimirTodosItensProducaoCommand.Execute(ARequestFilter);
  Result := FImprimirTodosItensProducaoCommand.Parameters[1].Value.GetBoolean;
end;

function TSrvServerMetodosClient.ImprimirConta(Comanda: string;
  const ARequestFilter: string): Boolean;
begin
  if FImprimirContaCommand = nil then
  begin
    FImprimirContaCommand := FConnection.CreateCommand;
    FImprimirContaCommand.RequestType := 'GET';
    FImprimirContaCommand.Text := 'TSrvServerMetodos.ImprimirConta';
    FImprimirContaCommand.Prepare(TSrvServerMetodos_ImprimirConta);
  end;
  FImprimirContaCommand.Parameters[0].Value.SetWideString(Comanda);
  FImprimirContaCommand.Execute(ARequestFilter);
  Result := FImprimirContaCommand.Parameters[1].Value.GetBoolean;
end;

constructor TSrvServerMetodosClient.Create(ARestConnection: TDSRestConnection);
begin
  inherited Create(ARestConnection);
end;

constructor TSrvServerMetodosClient.Create(ARestConnection: TDSRestConnection;
  AInstanceOwner: Boolean);
begin
  inherited Create(ARestConnection, AInstanceOwner);
end;

destructor TSrvServerMetodosClient.Destroy;
begin
  FClientSocket1ReadCommand.DisposeOf;
  FEchoStringCommand.DisposeOf;
  FReverseStringCommand.DisposeOf;
  FGetGruposCommand.DisposeOf;
  FGetGruposCommand_Cache.DisposeOf;
  FGetUsuariosCommand.DisposeOf;
  FGetUsuariosCommand_Cache.DisposeOf;
  FGetFavoritosCommand.DisposeOf;
  FGetFavoritosCommand_Cache.DisposeOf;
  FGetProdutosCommand.DisposeOf;
  FGetProdutosCommand_Cache.DisposeOf;
  FEnviarBDCommand.DisposeOf;
  FEnviarBDCommand_Cache.DisposeOf;
  FPutProdutosCommand.DisposeOf;
  FGetContaCommand.DisposeOf;
  FGetContaCommand_Cache.DisposeOf;
  FValidaLoginCommand.DisposeOf;
  FValidaMesaCommand.DisposeOf;
  FTrocaMesaCommand.DisposeOf;
  FTransferirItemMesaCommand.DisposeOf;
  FImprimirItemProducaoCommand.DisposeOf;
  FImprimirTodosItensProducaoCommand.DisposeOf;
  FImprimirContaCommand.DisposeOf;
  inherited;
end;

end.
