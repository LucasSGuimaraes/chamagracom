unit uFrmPrincipal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl,
  FMX.StdCtrls, FMX.Controls.Presentation,
  FMX.Gestures, System.Actions, FMX.ActnList, System.ImageList, FMX.ImgList,
  FMX.ListView.Types, FMX.ListView.Appearances, FMX.ListView.Adapters.Base,
  FMX.Objects, FMX.ListView, Data.DB, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Rtti,
  System.Bindings.Outputs, FMX.Bind.Editors, Data.Bind.EngExt,
  FMX.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope,
  FGX.ProgressDialog, Data.FireDACJSONReflect,
  Data.DBXJSONReflect;

type
  TfrmPrincipal = class(TForm)
    ctlPrincipal: TTabControl;
    tabChamaGracom: TTabItem;
    barChamaGarcom: TToolBar;
    lblTituloChamaGracom: TLabel;
    tabCardapio: TTabItem;
    barCardapio: TToolBar;
    lblTituloCardapio: TLabel;
    tabConta: TTabItem;
    barConta: TToolBar;
    lblTituloConta: TLabel;
    gstGerenteGestos: TGestureManager;
    lstAcoes: TActionList;
    NextTabAction1: TNextTabAction;
    PreviousTabAction1: TPreviousTabAction;
    lstImagens: TImageList;
    StyleBook: TStyleBook;
    ctlGruposCardapio: TTabControl;
    tabGrupos: TTabItem;
    lvwGrupos: TListView;
    tabProdutos: TTabItem;
    lvwProdutos: TListView;
    tabFavoritos: TTabItem;
    lvwContas: TListView;
    recBot: TRectangle;
    btnPedirConta: TRectangle;
    lblPedirConta: TLabel;
    Rectangle1: TRectangle;
    Label1: TLabel;
    recItensTotal: TRectangle;
    lblTotal: TLabel;
    lblItensConsumidos: TLabel;
    Label3: TLabel;
    memTempConta: TFDMemTable;
    memTempContaID: TIntegerField;
    memTempContaID_VENDA: TIntegerField;
    memTempContaCOMANDA: TIntegerField;
    memTempContaMESA: TIntegerField;
    memTempContaNITEM: TIntegerField;
    memTempContaPRODUTO: TIntegerField;
    memTempContaDESCRICAO: TStringField;
    memTempContaQUANTIDADE: TBCDField;
    memTempContaPRECO_UNIT: TBCDField;
    memTempContaVALOR_TOTAL_PRODUTO: TBCDField;
    memTempContaACRESCIMO: TBCDField;
    memTempContaDESCONTO: TBCDField;
    memTempContaDATA: TDateField;
    memTempContaHORA: TTimeField;
    memTempContaHORA_ENTREGUE: TTimeField;
    memTempContaSETOR: TIntegerField;
    memTempContaIMPRESSO: TStringField;
    memTempContaNAO_BAIXA_ESTOQUE: TStringField;
    memTempContaVENDEDOR: TIntegerField;
    memTempContaOBS_PREFERENCIA: TStringField;
    memTempContaUSUARIO: TStringField;
    memTempContaCANCELADO: TStringField;
    memTempContaC_USUARIO: TStringField;
    memTempContaC_DATA: TDateField;
    memTempContaC_HORA: TTimeField;
    memTempContaSTATUS: TStringField;
    memTempContaNAO_ESTOQUE: TStringField;
    memTempContaCOMANDA_ANTERIOR: TIntegerField;
    memTempContaC_AUTORIZACAO: TStringField;
    memTempContaGARCON_FIXO: TStringField;
    memTempContaTX_ENTREGA: TBCDField;
    memTempContaIP: TStringField;
    memTempGrupos: TFDMemTable;
    memTempProdutos: TFDMemTable;
    memTempFavoritos: TFDMemTable;
    memTempProdutosPRODUTO: TWideStringField;
    memTempProdutosDESCRICAO: TStringField;
    memTempProdutosGRUPO: TIntegerField;
    memTempProdutosPRECO: TBCDField;
    memTempProdutosBARRA: TStringField;
    memTempProdutosOBS: TWideMemoField;
    memTempProdutosTAXA_ENTREGA: TBCDField;
    memTempProdutosFAVORITO: TIntegerField;
    memTempProdutosFOTO: TBlobField;
    memTempProdutosGRADE: TStringField;
    memTempGruposGRUPO: TIntegerField;
    memTempGruposDESCRICAO: TStringField;
    memTempGruposFOTO: TBlobField;
    memTempFavoritosPRODUTO: TWideStringField;
    memTempFavoritosDESCRICAO: TStringField;
    memTempFavoritosGRUPO: TIntegerField;
    memTempFavoritosPRECO: TBCDField;
    memTempFavoritosBARRA: TStringField;
    memTempFavoritosOBS: TWideMemoField;
    memTempFavoritosTAXA_ENTREGA: TBCDField;
    memTempFavoritosFAVORITO: TIntegerField;
    memTempFavoritosFOTO: TBlobField;
    memTempFavoritosGRADE: TStringField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    BindSourceDB2: TBindSourceDB;
    BindSourceDB3: TBindSourceDB;
    LinkListControlToField3: TLinkListControlToField;
    BindSourceDB4: TBindSourceDB;
    LinkListControlToField4: TLinkListControlToField;
    lvwFavoritos: TListView;
    LinkListControlToField5: TLinkListControlToField;
    procedure GestureDone(Sender: TObject; const EventInfo: TGestureEventInfo;
      var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure ctlPrincipalChange(Sender: TObject);
  private
    Fcomanda: Integer;
    procedure Setcomanda(const Value: Integer);
    procedure ShowChamaGarcom;
    procedure ShowCardapio;
    procedure ShowConta;
    procedure PreencheMemoryTable(LDataSetList: TFDJSONDataSets;
      MemTable: TFDMemTable);
    { Private declarations }
  public
    { Public declarations }
    vTabTmp: TFDMemTable;
    property comanda: Integer read Fcomanda write Setcomanda;
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.fmx}

uses
  uDM, FGX.Toasts;

procedure TfrmPrincipal.ctlPrincipalChange(Sender: TObject);
begin
  case ctlPrincipal.ActiveTab of
    tabChamaGracom:
      ShowChamaGarcom;
    tabCardapio:
      ShowCardapio;
    tabConta:
      ShowConta;
  end;
end;

procedure TfrmPrincipal.FormCreate(Sender: TObject);
begin
  { This defines the default active tab at runtime }
  ctlPrincipal.ActiveTab := tabChamaGracom;
end;

procedure TfrmPrincipal.GestureDone(Sender: TObject;
  const EventInfo: TGestureEventInfo; var Handled: Boolean);
begin
  case EventInfo.GestureID of
    sgiLeft:
      begin
        if ctlPrincipal.ActiveTab <> ctlPrincipal.Tabs[ctlPrincipal.TabCount - 1]
        then
          ctlPrincipal.ActiveTab := ctlPrincipal.Tabs
            [ctlPrincipal.TabIndex + 1];
        Handled := True;
      end;

    sgiRight:
      begin
        if ctlPrincipal.ActiveTab <> ctlPrincipal.Tabs[0] then
          ctlPrincipal.ActiveTab := ctlPrincipal.Tabs
            [ctlPrincipal.TabIndex - 1];
        Handled := True;
      end;
  end;
end;

procedure TfrmPrincipal.Setcomanda(const Value: Integer);
begin
  Fcomanda := Value;
end;

procedure TfrmPrincipal.ShowCardapio;
begin
  if (memTempProdutos.RecordCount > 0) or (memTempGrupos.RecordCount > 0) or
    (memTempFavoritos.RecordCount > 0) then
    Exit;

  PreencheMemoryTable(DM.SrvServerMetodosClient.GetProdutos, memTempProdutos);

  PreencheMemoryTable(DM.SrvServerMetodosClient.GetGrupos, memTempProdutos);

  PreencheMemoryTable(DM.SrvServerMetodosClient.GetFavoritos, memTempProdutos);
end;

procedure TfrmPrincipal.ShowChamaGarcom;
begin

end;

procedure TfrmPrincipal.ShowConta;
var
  LDataSetList: TFDJSONDataSets;
  Total: Currency;
  ItensConsulmidos, intNum: Integer;
begin
  inherited;

  if (memTempConta.RecordCount > 0) then
    Exit;

  Total := 0.0;
  ItensConsulmidos := 0;

  LDataSetList := DM.SrvServerMetodosClient.GetConta(comanda);
  memTempConta.Active := False;
  Assert(TFDJSONDataSetsReader.GetListCount(LDataSetList) = 1);
  memTempConta.AppendData(TFDJSONDataSetsReader.GetListValue(LDataSetList, 0));

  memTempConta.First;
  intNum := memTempConta.RecordCount;
  if intNum > 0 then
  begin
    while not memTempConta.Eof do
    begin
      Total := Total + memTempConta.FieldByName('VALOR_TOTAL_PRODUTO')
        .AsCurrency;
      ItensConsulmidos := ItensConsulmidos + memTempConta.FieldByName
        ('QUANTIDADE').AsInteger;

      memTempConta.Next;
    end;
  end
  else
  begin
    TfgToast.Show('Conta vazia!');
    Close;
  end;

  lblTotal.Text := 'R$ ' + CurrToStr(Total);
  lblItensConsumidos.Text := 'Itens Consumidos: ' + IntToStr(ItensConsulmidos);
end;

procedure TfrmPrincipal.PreencheMemoryTable(LDataSetList: TFDJSONDataSets;
  MemTable: TFDMemTable);
begin
  MemTable.Active := False;
  Assert(TFDJSONDataSetsReader.GetListCount(LDataSetList) = 1);
  MemTable.AppendData(TFDJSONDataSetsReader.GetListValue(LDataSetList, 0));
end;

end.
